﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LAB_7
{
    class Program
    {
        static void Main(string[] args)
        {
            int a;
            string tmp;
            //double[] F = { 1, 2, 4.3, 9.3, -7, -12, 5 };
            //Console.WriteLine("Mass = ");
            //for (int i = 0; i < F.Length; i++) Console.Write(F[i] + " ");
            //Console.WriteLine(System.Environment.NewLine);
            //Console.WriteLine("Сума вiдємних елементiв =  {0}", MinusElMas(F));
            //Console.WriteLine("Максимальний елемент =  {0}", MaxElMas(F));
            //Console.WriteLine("Iндекс максимального елемента =  {0}", MaxElMasNum(F));
            //Console.WriteLine("Максимальний за модулем елемент =  {0}", MaxModulElMas(F));
            //Console.WriteLine("Суму iндексiв додатних елементiв =  {0}", SumOfIndexMoreThenZero(F));
            //Console.WriteLine("Кiлькiсть цiлих чисел у масивi. =  {0}", IntInDob(F));


            //double[] F = { 1, 2, 0, 3.2, 10, 0, -17, 3, -2 };
            //for (int i = 0; i < F.Length; i++) Console.Write(F[i] + " ");
            //Console.WriteLine(System.Environment.NewLine);
            //Console.WriteLine("Добуток елементiв пiсля мiнiмального елементу =  {0}", DobAfMin(F));
            //Console.WriteLine("Cуму елементiв масиву, що розташованi мiж першим вiд'ємним та другим додатним елементами =  {0}", Betvin(F));
            //Console.WriteLine("Cуму елементiв масиву, якi розташованi мiж першим i останнiм нульовими елементами =  {0}", BetvinZero(F));
            //Console.WriteLine("Добуток елементiв масиву, що розташованi мiж максимальним за модулем i мiнiмальним за модулем елементами =  {0}", BetvinMaxMinModul(F));
        }


        static double MinusElMas(double[] A)
        {
            double size = 0;
            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] < 0) size += A[i];
            }
            return size;
        }

        static double MaxElMas(double[] A)
        {
            double size = A[0];
            for (int i = 0; i < A.Length; i++)
            {
                if (size < A[i]) size = A[i];
            }
            return size;
        }

        static double MaxElMasNum(double[] A)
        {
            double size = A[0], index = 0;
            for (int i = 0; i < A.Length; i++)
            {
                if (size < A[i]) { size = A[i]; index = i; }
            }
            return index;
        }

        static double MaxModulElMas(double[] A)
        {
            double size = Math.Abs(A[0]);
            for (int i = 0; i < A.Length; i++)
            {
                if (size < Math.Abs(A[i])) size = Math.Abs(A[i]);
            }
            return size;
        }

        static double SumOfIndexMoreThenZero(double[] A)
        {
            double index = 0;
            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] > 0) index += i;
            }
            return index;
        }

        static double IntInDob(double[] A)
        {
            double index = 0;
            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] % 1 == 0) index++;
            }
            return index;
        }


        static double DobAfMin(double[] A)
        {
            double min = A[0], res = 1;
            int index = 0;
            for (int i = 0; i < A.Length; i++)
            {
                if (min > A[i]) { min = A[i]; index = i; }
            }
            for (int i = index + 1; i < A.Length; i++)
            {
                res *= A[i];
            }
            return res;
        }

        static double Betvin(double[] A)
        {
            double min = A[0], res = 0;
            int indexMin = 0, indexMax = 0, lic = 0, licout;
            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] < 0) { indexMin = i; break; }
            }
            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] > 0) { lic++; if (lic == 2) { indexMax = i; break; } }
            }
            if (indexMin > indexMax) { lic = indexMax; licout = indexMin; }
            else { lic = indexMin; licout = indexMax; }
            for (int i = lic + 1; i < licout; i++)
            {
                res += A[i];
            }
            return res;
        }

        static double BetvinZero(double[] A)
        {
            double min = A[0], res = 0;
            int indexMin = 0, indexMax = 0;
            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] == 0) { indexMin = i; break; }
            }
            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] == 0) { indexMax = i; }
            }
            for (int i = indexMin + 1; i < indexMax; i++)
            {
                res += A[i];
            }
            return res;
        }

        static double BetvinMaxMinModul(double[] A)
        {
            double min = A[0], max = A[0], res = 0;
            int indexMin = 0, indexMax = 0, lic = 0, licout;
            for (int i = 0; i < A.Length; i++)
            {
                if (Math.Abs(A[i]) < min) { min = Math.Abs(A[i]); indexMin = i; }
            }
            for (int i = 0; i < A.Length; i++)
            {
                if (Math.Abs(A[i]) > max) { max = Math.Abs(A[i]); indexMax = i; }
            }
            if (indexMin > indexMax) { lic = indexMax; licout = indexMin; }
            else { lic = indexMin; licout = indexMax; }
            for (int i = lic + 1; i < licout; i++)
            {
                res += A[i];
            }
            return res;
        }
        }
    }
